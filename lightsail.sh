#!/bin/bash
#this script will run when creating instance

# install latest version of docker the lazy way
curl -sSL https://get.docker.com | sh

# make it so you don't need to sudo to run docker commands, using user ubuntu, which is default user in lightsail
sudo usermod -aG docker ubuntu

# install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# copy the dockerfile into /srv/docker 
#i included this file so docker will start immediately after system restart, we dont have to manually start container after system restart
# WorkingDirectory=[whatever you have below]
mkdir /srv/docker
curl -o ~/docker-ajocard/docker-compose.yml https://bitbucket.org/dapseen/docker-test/raw/22b35c3ca74a4029913ecd9fcb7117eba834920a/docker-compose.yml

# copy in systemd unit file and register it so our compose file runs 
# on system restart
#Please refer to comment on line 15, i used this file to run the restart
curl -o /etc/systemd/system/docker-compose-app.service https://bitbucket.org/dapseen/docker-test/raw/44ecdb89f1970214165ec483cbd18a5fc716935d/docker-compose-app.service
systemctl enable docker-compose-app

# start up the application via docker-compose
docker-compose -f ~/docker-test/docker-compose.yml up -d